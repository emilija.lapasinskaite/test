#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <pthread.h>
#include <nng/nng.h>
#include <nng/protocol/reqrep0/req.h>
#define NUMTHREADS 3
char *ports[NUMTHREADS];

void *thr(void *threadid){

	int portid;
	portid = threadid;
	printf("Thread %d addr: %s\n", portid, ports[portid]);

    nng_socket sock; 
    int rv;

	if ((rv = nng_req0_open(&sock)) != 0) //creating socket
	{ 
		printf("socket not created, id: %d, err: %s \n", portid, nng_strerror(rv)); 
		exit(0); 
	} 
	else printf("socket created, id: %d\n", portid); 
	
    if ((rv = nng_dial(sock, ports[portid], NULL, NNG_FLAG_NONBLOCK)) != 0) //dialling to the server
	{ 
		printf("not dialled, id: %d, err: %s \n", portid,  nng_strerror(rv)); 
		exit(0); 
	} 
	else printf("dialled, id: %d\n", portid); 
    
     nng_msleep(3000);
    
    //sending and receiving the msg
    nng_msg *msgsend = NULL;
    nng_msg *msgrecv = NULL;
    const int32_t msg = "Please";

    if ((rv = nng_msg_alloc(&msgsend, sizeof(msg)))!=0) 
        printf("not allocated, id: %d, er: %s \n", portid,  nng_strerror(rv));
    else printf("allocated, id: %d\n", portid);

    if ((rv = nng_msg_append(msgsend, &msg, sizeof(msg)))!=0)
        printf("not appended, id: %d, err: %s \n", portid, nng_strerror(rv));
    else printf("appended, id: %d\n", portid);

     if ((rv = nng_sendmsg(sock, msgsend, NNG_FLAG_NONBLOCK))!=0)
     //if ((rv = nng_sendmsg(sock, msgsend, 0))!=0) //aka sending till the msg is sent; 
        printf("not sending, id: %d, err: %s\n", portid, nng_strerror(rv));
     else {
        printf("message sent, id: %d\n", portid);
        nng_msg_free(msgsend);
        }  

	if ((rv = nng_recvmsg(sock, &msgrecv, 0))!=0) //receiving only after the msg (request) was sent
        printf("not receiving, id: %d, err: %s \n", portid, nng_strerror(rv));
	else {
		printf("received: %s, id: %d\n", msgrecv, portid);
        nng_msg_free(msgrecv);
		}
}

int main() 
{ 
	 pthread_t threads[NUMTHREADS];
	 long portids[NUMTHREADS];
	 int rc, t;

	 ports[0] = "tcp://78.56.77.230:8125";
	 ports[1] = "tcp://78.56.77.230:8126";
	 ports[2] = "tcp://78.56.77.230:8127";

	 //creating threads
	 for (t=0; t<NUMTHREADS; t++) {
		 portids[t]=t;
		 printf("Creating thread %d\n", t); 
		 rc = pthread_create(&threads[t], NULL, thr, (void *)portids[t]);
		 if (rc){
			 printf("ERROR; return code from pthread_create is %d\n", rc);
			 exit(-1); 
			}
		}
    
	pthread_exit(NULL);
} 