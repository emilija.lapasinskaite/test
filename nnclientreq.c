#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <pthread.h>
#include <string.h>
#include <nanomsg/nn.h>
#include <nng/nng.h>
#include<nng/protocol/reqrep0/req.h>
//#include<nng/protocol/bus0/bus.h>
#include <errno.h>

//#define MAX 80 
//#define SA struct sockaddr 
//#define NUMTHREADS 3
//int client(const char *url)

int main()
{
    nng_socket sock; 
    int rv;

	if ((rv = nng_req0_open(&sock)) != 0) 
	{ 
		printf("socket not created, err: %s \n", nng_strerror(rv)); 
		exit(0); 
	} 
	else printf("socket created\n"); 
	
    //if ((rv = nng_dial(sock, "tcp://206.81.30.33:8226", NULL, NNG_FLAG_NONBLOCK)) != 0) 
    if ((rv = nng_dial(sock, "tcp://78.56.77.230:8125", NULL, NNG_FLAG_NONBLOCK)) != 0) 
	{ 
		printf("not dialled, err: %s \n", nng_strerror(rv)); 
		exit(0); 
	} 
	else printf("dialled\n"); 
    
     nng_msleep(3000);
    
    nng_msg *msgsend = NULL;
    nng_msg *msgrecv = NULL;
    const int32_t msg = "Please";

    if ((rv = nng_msg_alloc(&msgsend, sizeof(msg)))!=0)
        printf("not allocated; er: %s \n", nng_strerror(rv));
    else printf("allocated\n");

    if ((rv = nng_msg_append(msgsend, &msg, sizeof(msg)))!=0)
        printf("not appended; er: %s \n", nng_strerror(rv));
    else printf("appended\n");

    if ((rv = nng_sendmsg(sock, msgsend, NNG_FLAG_NONBLOCK))!=0)
     //if ((rv = nng_sendmsg(sock, msgsend, 0))!=0) //aka sending till the msg is sent; 
        printf("not sending, err: %s\n", nng_strerror(rv));
     else {
        printf("message sent\n");
        nng_msg_free(msgsend);
        }  

	if ((rv = nng_recvmsg(sock, &msgrecv, 0))!=0) 
        printf("not receiving, err: %s \n",nng_strerror(rv)); // wrong state if msg´s not sent;
	else {
		printf("received: %s\n", msgrecv);
        nng_msg_free(msgrecv);
		}

	//nng_close(sock);
    return(0);
} 