#include <netdb.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <pthread.h>
#define MAX 80 
//int PORT[4]={8080, 8125, 8126, 8127};
#define SA struct sockaddr 
#define NUMTHREADS 3
char *ports[NUMTHREADS];

void func(int sockfd) 
{ 
	char buff[MAX]; 
	int n; 
	for (;;) { 
		bzero(buff, sizeof(buff)); 
		printf("Your message: \n"); 
		n = 0; 
        int m = 0;

		//sending and receiving the message

		while ((buff[n++] = getchar()) != '\n');
            //printf("dydis: %i\n", strlen(buff));
            int l = strlen(buff);
            char buff1[l-1];
        while (m<l) {
            buff1[m]=buff[m];
            m++;
        }
           // printf("tekstukas buff1: %s\n", buff1);

            if ((strncmp(buff, "exit", 4)) == 0) 
			{ 
			strcpy(buff, "Client has left the conversation. Cheers!\n");
			write(sockfd, buff, sizeof(buff)); 
			printf("You´re done now; Cheers!\n");
			break; 
			}
        //printf("size: %i\n", sizeof(buff1));
		write(sockfd, buff1, sizeof(buff1)); 
		bzero(buff, sizeof(buff));
        bzero(buff1, sizeof(buff1));
		read(sockfd, buff, sizeof(buff)); 
		printf("Server says: %s\n", buff); 
		
	} 
} 

void *thr(void *threadid){

	long portid;
	portid = (long) threadid;
	printf("Thread %d port: %d\n", portid, ports[portid]);

    int sockfd, connfd; 
	struct sockaddr_in servaddr, cli; 

	// socket creation and varification 
	sockfd = socket(AF_INET, SOCK_STREAM, 0); 
	if (sockfd == -1) 
	{ 
		printf("socket creation failed\n"); 
		exit(0); 
	} 
	else
		printf("Socket successfully created, port: %d\n", ports[portid]); 
	bzero(&servaddr, sizeof(servaddr)); 

	// assigning IP, PORT

	servaddr.sin_family = AF_INET; 
    //servaddr.sin_addr.s_addr = inet_addr("206.81.30.33");
	//servaddr.sin_addr.s_addr = inet_addr("78.56.77.230"); 
	servaddr.sin_port = htons(ports[portid]); 

	// connecting the client socket to server socket 
	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) 
	{ 
		printf("connection with the server failed, port: %d\n", ports[portid]); 
		exit(0); 
	} 
	else
		printf("connected to the server, port: %d\n", ports[portid]); 

	// function for chat 
	func(sockfd); 

	// close the socket 
	close(sockfd);

	pthread_exit(NULL);


}

int main() 
{ 
	 pthread_t threads[NUMTHREADS];
	 long portids[NUMTHREADS];
	 int rc, t;

	 ports[0] = 8125;
	 ports[1] = 8126;
	 ports[2] = 8127;

	 for (t=0; t<NUMTHREADS; t++) {
		 portids[t]=t;
		 printf("Creating thread %d\n", t); 
		 rc = pthread_create(&threads[t], NULL, thr, (void *)portids[t]);
		 if (rc){
			 printf("ERROR; return code from pthread_create is %d\n", rc);
			 exit(-1); 
		 }
	 }
	pthread_exit(NULL);
} 
