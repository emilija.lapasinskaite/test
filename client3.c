#include <netdb.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <errno.h>
#include <unistd.h>
#define MAX 1000
#define PORT 5555
#define SA struct sockaddr 

int main() 
{   
    FILE * file;
    int ch=0, i=0, j;

    //counting characters in the .txt
    if ((file=fopen("text.txt", "r"))==NULL) 
        printf("error opening file");
    char m;
    while(!(feof(file))) {
        m = getc(file);
        ch++;  
    }
    fclose(file);
    
    ch=ch-1;
    char text[ch];

    // storing text from .txt in the array
    if ((file=fopen("text.txt", "r"))==NULL) 
        printf("error opening file pt2");
    while(!(feof(file))) {
        text[i] = getc(file);
        i++;  
    }
    fclose(file);

    //creating socket, connecting 
    int sockfd;
	struct sockaddr_in servaddr, cli; 

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0))==-1)
	{ 
		printf("Socket creation failed\n"); 
		exit(0); 
	} 
	else printf("Socket successfully created\n"); 
	
    bzero(&servaddr, sizeof(servaddr)); 

	servaddr.sin_family = AF_INET; 
	servaddr.sin_addr.s_addr = inet_addr("78.56.77.230"); 
	servaddr.sin_port = htons(PORT); 

	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) 
	{ 
		printf("connection with the server failed\n"); 
		exit(0); 
	} 
	else printf("connected to the server\n\n"); 

    int ch1 = ch - (int)(ch/12)*12; //remainder
    char part[12], part1[ch1], buff[MAX]; //arrays for storing parts of the text, received msg
    
    //sending and receiving messages (parts of text, 12 characters)
    for (i=0; i<ch-ch1; i=i+12){
        for(j=0; j<12; j++) 
            part[j]=text[i+j];
        // if (strlen(part)!=12)
        //     printf("\n smth went wrong:( i:%d, size: %ld \n\n", i, strlen(part));
        if ((send(sockfd, part, 12, 0))<=0) 
            printf("not sending'\n");
        recv(sockfd, buff, sizeof(buff), 0); 
        printf("%s", buff);
        bzero(buff, sizeof(buff));          
    }
    //sending the remaining part of the text
    for (i=0; i<ch1; i++) 
        part1[i]=text[i+ch-ch1];
	if ((send(sockfd, part1, ch1, 0))<=0) 
        printf("not sending'\n");
    recv(sockfd, buff, sizeof(buff), 0);         
    printf("%s\n\n", buff);
    bzero(buff, sizeof(buff));  

    close(sockfd);
    return(0);
} 